pacman:: p_load(pacman,rio,tidyverse)
library(datasets) #loading base packages


?uspop
uspop

plot(uspop) #default way to plot the data

uspop %>%
  plot(
    main = "US Population 1790-1970",
    sub = "(Source: datasets::uspop)",
    xlab = "Year",
    ylab = "Population in millions"
      )
# for reference in 1930 line
abline(v = 1930, col = "lightgray")
# abline(h = 100, col = "lightgray")
text(1930, 10,"1930", col = "red3")

# for reference line in 1940
abline(v = 1940, col = "lightgray")
text(1940, 20,"1940", col = "red3")
# ?abline


# time series plot 
?ts.plot
ts.plot(uspop)

?plot.ts
plot.ts(uspop)
  
# now about EU stock market
plot(EuStockMarkets)
plot.ts(EuStockMarkets)
ts.plot(EuStockMarkets)

iris
iris %>%
  select(Sepal.Length,Sepal.Width) %>%
  ts.plot(
    # main = "Petal length"
    )
abline(v = 5, col = "red")
# ts.plot(iris)

rm(list=ls())

